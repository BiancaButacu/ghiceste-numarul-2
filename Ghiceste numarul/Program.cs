﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ghiceste_numarul
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int numarAleator = rand.Next(0, 100);
            int numarUtilizator;
            int numarIncercari = 3;
            bool jocTerminat = false;

            Console.WriteLine("Ghiceste numarul secret! Introduceti un numar de la 0 la 100 (Numarul aleator este: " + numarAleator +")");
            while (jocTerminat==false)
            {   
                numarUtilizator= Convert.ToInt32(Console.ReadLine());
                numarIncercari--;
                if (numarUtilizator!=numarAleator && numarIncercari==3)
                {
                    Console.WriteLine("Din pacate nu ai ghicit numarul! Ne pare rau :(");
                    jocTerminat = true;

                }
                else if (numarUtilizator==numarAleator)
                {
                    Console.WriteLine("Felicitari!Ai ghicit numarul! ");
                    jocTerminat = true;
                }
                else if ((numarUtilizator> numarAleator && numarUtilizator-numarAleator <=3) || (numarUtilizator<numarAleator && numarAleator-numarUtilizator <=3))
                {
                    Console.WriteLine("Foarte fierbinte");
                }
                else if ((numarUtilizator > numarAleator && numarUtilizator - numarAleator <= 5) || (numarUtilizator < numarAleator && numarAleator - numarUtilizator <= 5))
                {
                    Console.WriteLine("Fierbinte");
                }
                else if ((numarUtilizator > numarAleator && numarUtilizator - numarAleator <= 10) || (numarUtilizator < numarAleator && numarAleator - numarUtilizator <= 10))
                {
                    Console.WriteLine("Cald");
                }
                else if ((numarUtilizator > numarAleator && numarUtilizator - numarAleator <= 20) || (numarUtilizator < numarAleator && numarAleator - numarUtilizator <= 20))
                {
                    Console.WriteLine("Caldut");
                }
                else if ((numarUtilizator > numarAleator && numarUtilizator - numarAleator <= 50) || (numarUtilizator < numarAleator && numarAleator - numarUtilizator <=50))
                {
                    Console.WriteLine("Rece");
                }
                else if ((numarUtilizator > numarAleator && numarUtilizator - numarAleator >50) || (numarUtilizator < numarAleator && numarAleator - numarUtilizator >50))
                {
                    Console.WriteLine("Foarte rece");
                }
                
            }
            Console.ReadKey();
        }
    }
}
